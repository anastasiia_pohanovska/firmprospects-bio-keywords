package model;

import lombok.Data;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author andrey_pylypchuk.
 */
@Data
public class SubDataBiography {
    private String fullBio;
    private String prevFullBio;
    private String url;
    private Set<String> keywordAddToNewBio;
    private Set<String> keywordRemoveFromNewBio;

    public String getKeywordAddToNewBioAsString() {
        return keywordAddToNewBio.stream().map(String::toString).collect(Collectors.joining(", "));
    }

    public String getKeywordRemoveFromNewBioAsString() {
        return keywordRemoveFromNewBio.stream().map(String::toString).collect(Collectors.joining(", "));
    }
}
