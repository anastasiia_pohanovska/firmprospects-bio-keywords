import model.SubDataBiography;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import services.read.excel.ExcelReaderService;
import services.read.excel.FileUtilService;
import services.search.keywords.KeywordsSearchService;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static services.UpdateWorkbookService.updateWorkbook;

/**
 * @author foxy
 * @since 26.02.18.
 */
public class Main {

    public static void main(String[] args) throws IOException, InvalidFormatException {
        String fileName = args[0];

        XSSFWorkbook workbook = (XSSFWorkbook) FileUtilService.openWorkbook(fileName);

        ExcelReaderService service = new ExcelReaderService();

        List<SubDataBiography> biographyList = service.readSubDataBiography(workbook);

        for (int i = 0; i < biographyList.size(); i++) {
            Set<String> keywordSetFromPreviousBio = KeywordsSearchService.findKeywordInBio(biographyList.get(i).getPrevFullBio());
            Set<String> keywordSetFromNewBio = KeywordsSearchService.findKeywordInBio(biographyList.get(i).getFullBio());
            Set<String> keywordSetAddToNewBio = new HashSet<>();
            Set<String> keywordSetRemovedFromNewBio = new HashSet<>();
            for (String wordFromNewBio : keywordSetFromNewBio) {
                if (!keywordSetFromPreviousBio.contains(wordFromNewBio)) {
                    keywordSetAddToNewBio.add(wordFromNewBio);
                }
            }
            if (keywordSetAddToNewBio.isEmpty()) {
                keywordSetAddToNewBio.add("-");
            }
            biographyList.get(i).setKeywordAddToNewBio(keywordSetAddToNewBio);
            for (String wordFromPreviousBio : keywordSetFromPreviousBio) {
                if (!keywordSetFromNewBio.contains(wordFromPreviousBio)) {
                    keywordSetRemovedFromNewBio.add(wordFromPreviousBio);
                }
            }
            if (keywordSetRemovedFromNewBio.isEmpty()) {
                keywordSetRemovedFromNewBio.add("-");
            }
            biographyList.get(i).setKeywordRemoveFromNewBio(keywordSetRemovedFromNewBio);
        }

        updateWorkbook(biographyList, workbook);

        FileUtilService.closeWorkbook(fileName, workbook);
    }
}
