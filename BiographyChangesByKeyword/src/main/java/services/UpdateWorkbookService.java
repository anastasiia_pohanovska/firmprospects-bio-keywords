package services;

import model.SubDataBiography;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;

/**
 * @author andrey_pylypchuk.
 */
public class UpdateWorkbookService {

    public static Workbook updateWorkbook(List<SubDataBiography> biographyList, XSSFWorkbook workbook) {
        final XSSFSheet sheet = workbook.getSheetAt(0);
        Row firstRow = sheet.getRow(0);
        int countOfCell = firstRow.getPhysicalNumberOfCells();

        for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {
            Row row = sheet.getRow(i);
            Cell cell;
            if (i == 0) {
                cell = row.createCell(countOfCell);
                cell.setCellValue("Profile Url Test");

                cell = row.createCell(countOfCell + 1);
                cell.setCellValue("Previous Bio Test");

                cell = row.createCell(countOfCell + 2);
                cell.setCellValue("New Bio Test");

                cell = row.createCell(countOfCell + 3);
                cell.setCellValue("Added Keywords");

                cell = row.createCell(countOfCell + 4);
                cell.setCellValue("Removed Keywords");
                continue;
            }

            SubDataBiography biography = biographyList.get(i - 1);

            cell = row.createCell(countOfCell);
            cell.setCellValue(biography.getUrl());

            cell= row.createCell(countOfCell + 1);
            cell.setCellValue(biography.getPrevFullBio());

            cell = row.createCell(countOfCell + 2);
            cell.setCellValue(biography.getFullBio());

            cell = row.createCell(countOfCell + 3);
            cell.setCellValue(biography.getKeywordAddToNewBioAsString());

            cell = row.createCell(countOfCell + 4);
            cell.setCellValue(biography.getKeywordRemoveFromNewBioAsString());
        }

        return workbook;
    }
}
