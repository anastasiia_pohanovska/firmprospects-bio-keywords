package services.search.keywords;

import java.util.*;

/**
 * @author foxy
 * @since 27.02.18.
 */
public class KeywordsSearchService {
    private static final List<String> KEYWORD_LIST = Arrays.asList(
            "Antitrust", "Banking", "Bankruptcy", "Corporate", "Energy", "Environmental", "ERISA", "Government",
            "Health Care", "Insurance", "Intellectual Property", "Labor & Employment", "Litigation", "Real Estate",
            "Tax", "Transportation", "Trusts & Estates", "International Trade", "Media", "FDA", "Entertainment",
            "Telecommunications", "40 Act", "Advertising", "Arbitration", "Asia", "Aviation", "Broker-Dealer",
            "Capital Markets", "Chapter 11", "Chapter 7", "Class Action", "Commodities", "Compliance/Regulatory",
            "Condo Associations", "Construction", "Consumer Finance", "Contracts", "Copyright", "Workers Compensation",
            "Criminal", "Cross-Border", "Data Privacy", "Deferred Compensation", "Derivatives", "Distressed Debt",
            "Dodd-Frank", "E-Commerce", "E-Discovery", "EEOC", "Emerging Growth", "Eminent Domain", "Employee Benefits",
            "Entertainment", "Equity Based Compensation", "ESOPs", "Exec Comp", "FCC", "Finance", "Franchise",
            "Hedge Funds", "Hotels & Resorts", "Immigration", "Incentive Plans", "Investigations",
            "Investment Management", "Land Use", "Latin America", "Leasing", "Legislative", "Leveraged Finance",
            "Licensing", "Lobbying", "Maritime", "Medicaid", "Medicare", "M&A", "Mezzanine Finance", "Oil & Gas",
            "OSHA", "Outsourcing", "CR/TM Litigation", "Patent Prosecution", "Payment Systems", "Pension Plans",
            "Pharmaceuticals", "Private Equity", "Product Liability", "Project Finance", "Public Finance",
            "Public Housing", "Reinsurance", "REITs", "Renewables", "Restructuring", "Retirement Plans", "Securities",
            "Water/CWA", "Structured Finance", "Technology Transactions", "Trademark", "Venture Capital", "Wage & Hour",
            "Air/CAA", "Welfare Plans", "White Collar", "Zoning", "Transactional", "Debt Finance", "Lending",
            "Investment Funds", "Fund Formation", "Climate Change", "Waste", "Professional Liability", "Life Sciences",
            "Counseling", "RICO", "Federal", "SALT", "Controversy", "International", "Probate", "Education Law",
            "Acquisition Finance", "HIPAA", "Debt Capital Markets", "Equipment Finance", "Corporate Trust", "D&O",
            "Emissions", "IPOs", "Utilities", "Clean Tech", "Gaming", "FERC", "Project Development", "CWA", "ESA",
            "CERCLA", "RCRA", "Municipal Law", "Policy", "Native American Law", "Sanctions", "Conflicts/Ethics",
            "Stark", "Coverage", "Defense", "Education", "Trade Secrets", "Appellate", "Family Law", "P&S", "FCPA",
            "Funds", "Exempt/Charitable", "Credits", "Workouts", "Unfair Competition", "UCC", "Agribusiness",
            "Food/Beverage", "Reorganizations", "Anti-Dumping", "Export Controls", "Customs", "Brownfields",
            "Election Law", "ACA", "ANDA", "Development", "ADA", "FMLA", "Affirmative Action", "Administrative",
            "Affordable Housing", "Hospitality", "Aerospace", "Joint Ventures", "FTC", "FINRA", "FLSA",
            "False Claims Act", "False Advertising", "Economic Development", "Sports", "First Amendment",
            "Advertising/Marketing", "Public Company", "Secured Finance", "Unsecured Finance",
            "Commodities/Derivatives", "Investment Advisor", "Non-Profit", "SOX", "Patent Litigation",
            "Hatch Waxman", "NLRB", "Whistleblower", "Pro Bono", "Landlord/Tenant", "Distressed", "Title VII",
            "Toxic Torts", "Wireless", "Creditors Rights", "DOJ", "HSR", "Infrastructure", "E&O", "ITC",
            "Civil Rights", "Personal Injury", "Partnership", "FAA", "Railroad", "Wills", "Wealth Management",
            "Succession Planning", "Enforcement", "Medical Devices", "Consumer Products", "Art Law", "Satellite",
            "Broadcast", "Mergers", "Acquisitions", "Acquisition", "Disposition", "Associate", "Partner", "Attorney",
            "Counsel", "Member", "Principal", "shareholder", "chair", "co-chair", "managing", "Director"
    );

    public static Set<String> findKeywordInBio(String biography) {
        Scanner scanner = new Scanner(biography);
        Set<String> keywordSet = new HashSet<>();
        while (scanner.hasNext()) {
            String nextToken = scanner.next();
            for (String word : KEYWORD_LIST) {
                if (nextToken.equalsIgnoreCase(word)) {
                    keywordSet.add(word);
                }
            }
        }
        return keywordSet;
    }
}
