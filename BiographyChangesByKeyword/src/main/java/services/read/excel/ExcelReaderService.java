package services.read.excel;


import model.SubDataBiography;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author foxy
 * @since 26.02.18.
 */
public class ExcelReaderService {

    public List<SubDataBiography> readSubDataBiography(XSSFWorkbook workbook) throws IOException {
        final XSSFSheet sheet = workbook.getSheetAt(0);

        int newBioCellNum = getCellFieldNumber(sheet, "Full Bio");
        int profileUrlCellNum = getCellFieldNumber(sheet, "Profile URL Changed Bio Only Table", "Profile URL");
        int prevBioCellNum = getCellFieldNumber(sheet, "Previous Full Bio");

        return new ArrayList<SubDataBiography>() {
            {
                sheet.forEach(row -> {
                    if (row.getRowNum() != 0) add(mapSDB(row, profileUrlCellNum, prevBioCellNum, newBioCellNum));
                });
            }
        };
    }

    private SubDataBiography mapSDB(Row row, int profileUrlCellNum, int prevFullBioCellNum, int fullBioCellNum) {
        SubDataBiography biography = new SubDataBiography();

        biography.setUrl(getValue(row.getCell(profileUrlCellNum)));
        biography.setFullBio(getValue(row.getCell(fullBioCellNum)));
        biography.setPrevFullBio(getValue(row.getCell(prevFullBioCellNum)));

        return biography;
    }

    private int getCellFieldNumber(XSSFSheet sheet, String... fieldTitles) {
        Row headerRaw = sheet.getRow(0);
        for (int i = 0; i < headerRaw.getPhysicalNumberOfCells(); i++) {
            String header = getValue(headerRaw.getCell(i));
            for (String fieldTitle : fieldTitles) {
                if (Objects.equals(header, fieldTitle)) {
                    return i;
                }
            }
        }
        throw new RuntimeException("No found header with name " + String.join(" or ", fieldTitles));
    }

    private String getValue(Cell cell) {
        if (null == cell) return "";
        if (cell.getCellTypeEnum().equals(CellType.STRING))
            return cell.getStringCellValue();
        if (cell.getCellTypeEnum().equals(CellType.BLANK))
            return "";

        else return null;
    }
}
