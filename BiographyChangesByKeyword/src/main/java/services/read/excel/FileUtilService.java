package services.read.excel;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author andrey_pylypchuk.
 */
public class FileUtilService {
    public static Workbook openWorkbook(String fileName) {
        Workbook workbook = null;
        try (FileInputStream fis = new FileInputStream(fileName)) {
            return new XSSFWorkbook(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return workbook;
    }

    public static void closeWorkbook(String fileName, Workbook workbook) {
        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(fileName);
            workbook.write(outputStream);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
